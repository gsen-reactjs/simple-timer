![Logo](https://gitlab.com/uploads/-/system/project/avatar/41294727/Screenshot_from_2022-12-06_09-43-09.png)

# POMODORO TIMER

A simple pomodoro timer based on a tutorial video as seen [here](https://www.youtube.com/watch?v=B1tjrnX160k)
by @CodingWithDawid

## Features

- Start and Pause
- Settings screen
- Set work time and break time

##

- [Source code original](https://github.com/dejwid/react-pomodoro-timer)

- [Demo original](https://reactjs-pomodoro-timer.netlify.app)

## Dependencies

[React Circular Progressbar](https://www.npmjs.com/package/react-circular-progressbar/v/1.2.1)

`npm install --save react-circular-progressbar`

React Slider

`npm install react-slider`
