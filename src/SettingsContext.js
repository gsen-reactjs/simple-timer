import React from "react";
const defaultValue = 15;
const SettingsContext = React.createContext(defaultValue);

export default SettingsContext;
