import "./App.css";
import Settings from "./Settings";
import Timer from "./Timer";

import { useState } from "react";
import SettingsContext from "./SettingsContext";

export default function App() {
  const [showSettings, setShowSettings] = useState(false);
  //sets work minutes on change sliders
  const [workMinutes, setWorkMinutes] = useState(45);
  const [breakMinutes, setBreakMinutes] = useState(15);

  //if showsettings true then show settings else show timer
  return (
    <main>
      <SettingsContext.Provider
        value={{
          showSettings,
          setShowSettings,
          workMinutes,
          breakMinutes,
          setWorkMinutes,
          setBreakMinutes,
        }}
      >
        {showSettings ? <Settings /> : <Timer />}
      </SettingsContext.Provider>
    </main>
  );
}
